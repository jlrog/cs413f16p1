
[![Codeship Status for jrodriguezorjuela/cs413f16p1](https://codeship.com/projects/5cd4a230-0efe-0132-1978-5ad6f07ad273/status?branch=master)](https://codeship.com/projects/32534)

# Passed tests

![](https://bytebucket.org/jrodriguezorjuela/cs413f16p1/raw/bbf948632051840daf953e6025cbeda3bce53b81/doc/project1.png?token=95a461b54ef77f41fee1ecbd211aa13e847f5365)

This build will always fail because it has 2 examples of failing tests:
one based on the source code, and one based on an incorrect test.

# Learning Objectives

* Simple hello world example
* Building with Gradle (using the Gradle wrapper)
* Automated unit testing with JUnit
* Continuous integration with Travis

# System requirements

* Java 6 SDK or later

# Running the Application

On Linux or Mac OS X:

    $ ./gradlew run
	
On Windows:
	
	> gradlew run

# Running the Tests

On Linux or Mac OS X:

    $ ./gradlew test
	
On Windows:
	
	> gradlew test
